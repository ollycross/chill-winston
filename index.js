const { transports, format, createLogger } = require('winston');

const factory = (loggerLevel = 'info', file = null, opts = {}) => {
  const loggerTransports = [
    new transports.Console({
      prettyPrint: true,
      colorize: true,
    }),
  ];

  if (file) {
    loggerTransports.push(new transports.File({ file }));
  }

  const loggerFormat = opts.loggerFormat || format.combine(
    format.timestamp({
      format: 'YYYYMMDD-HH:mm:ss',
    }),
    format.colorize({
      message: true,
    }),
    format.printf(({
      timestamp,
      message,
      level,
      ...meta
    }) => `${timestamp}.${level.toUpperCase()}: ${message} ${JSON.stringify(meta, null, 2)}`),
  );

  const combinedTransports = [...loggerTransports, ...opts.loggerTransports || []];

  const combinedOpts = {
    ...{
      transports: combinedTransports,
      level: loggerLevel,
      format: loggerFormat,
    },
    ...opts.winston,
  };

  return createLogger(combinedOpts);
};

module.exports = factory;
