# Chill Winston #

A simple implementation of the [Winston Logger](https://www.npmjs.com/package/winston)



## Install ##

```
npm i [-S|D] winston
```

## Usage ##
```
const chillWinston = require('chill-winston');

// chillWinston(loggerLevel = 'info', file = null, opts = {})

const logger = chillWinston('debug', '/path/to/logfile.log', {})

logger.notice('My message', {
	my: 'addional',
	data: 'stuff',
});

```

### Params ###
`loggerLevel`: Loglevel for Winston to use. See https://www.npmjs.com/package/winston#logging-levels. Default: `'info'`

`logfile`: A file for Winston to log to.  If null, will only log to console. Default: `null`

`opts`: Options object.  Takes the following parameters.  Default: `{}`

- `loggerFormat`: Logger format to use.  See https://www.npmjs.com/package/winston#formats.  If not set, uses a colorized format with timestamp, message and data.
- `loggerTransports`: Additional tranpsorts to use. Will be appended to the base transports of `transports.Console` and `transports.File`
- `winston`: Additional options has to be passed through to the `winston` instance. See https://www.npmjs.com/package/winston#logging