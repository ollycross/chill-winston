#### 1.0.2 (2019-10-24)

##### Bug Fixes

* **logger:**  Incorrect transport format (3aae3613)

#### 1.0.1 (2019-10-24)

##### Documentation Changes

* **repo:**  Added repo to package.json (20a5c44b)

##### Bug Fixes

* **package:**
  *  missing dependency (dde6a8ad)
  *  broken JSON (ac2596a3)

